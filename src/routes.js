const express = require('express');													
const router = express.Router();													
const UserController = require('./User/controller');								
const ToDoController = require('../src/Todo/controller');							

//User POST End-Point 
router.post('/users', UserController.creatUser);									

//User GET By ID End-Point
router.get('/users/:id', UserController.getUser);									

//User GET End-Point
router.get('/users/', UserController.getAllUser);									

//User verifying with Email 
router.get('/users/verifyuser/:verify', UserController.confirmEmail);				

//Login End-Point
router.post('/login', UserController.login);										

//Todo POST End-Point 
router.post('/todo', ToDoController.createToDo);									

//Todo GET By ID End-Point
router.get('/todo/:id' , ToDoController.getToDo);									

//Todo GET End-Point
router.get('/todo', UserController.validToken,  ToDoController.getAllToDo);			

//Todo delete End-Point
router.delete('/todo/:id' , ToDoController.deleteToDo);								

//Todo PUT End-Point
router.put('/todo/:id' , ToDoController.updateToDo);								

//Forget Password POST End-Point
router.post('/forget' ,UserController.forgetPassword)                                

//Reset Password PATCH End-Point
router.patch('/reset/:id' , UserController.resetPassword)                              

module.exports = { router };