const jwt = require('jsonwebtoken')
const momgoose = require('mongoose');								
const express = require('express');									
const Route = require('./routes');		
require('dotenv').config()							
const app = express();												

//Database.
momgoose.connect('mongodb://localhost/To-Do-App', {					
	useNewUrlParser			: true,
	useUnifiedTopology		: true,
	useFindAndModify		: true,
})
	.then(() => console.log('connected to mongoDB...'))
	.catch(() => console.log('could not connecting mongoDB...'));


app.use(express.json());

//start API Setting
app.use('/api', Route.router); 													

//PORT Setting 
const port =  process.env.PORT												
app.listen(port, () => console.log(`litsening to port ${port}`));
