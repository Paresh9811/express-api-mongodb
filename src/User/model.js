const Joi = require('joi');
const mongoose = require('mongoose');

//Creating model for Registration called as User
const userSchema = new mongoose.Schema({
	name: {
		type		: String,
		minlength	: 5,
		maxlength	: 50,
		require		: true
	},

	email: {
		type		: String,
		minlength	: 5,
		maxlength 	: 50,
		require   	: true,
		// unique    : true 
	},

	password:{
		type      : String,
		minlength : 8,
		maxlength : 50,
		require   : true
	},

	status :{
		type 	: String,
		default : "Unverified"
	},

	unique_id:{
		type : String
	},
	
	created_at:{
		type 	: String,
		default : Date
	}
	});


const User = mongoose.model('User' , userSchema);

//Validating function or validating fields...		
const validate = function validateUser(user){
	const schema = Joi.object ({
		name        : Joi.string().min(5).max(50).required(),
		email       : Joi.string().min(5).max(50).required().email(),
		password    : Joi.string().min(8).max(50).required()
	});
return schema.validate(user)
};

module.exports = {
	User,
	validate
}