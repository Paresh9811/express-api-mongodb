const jwt = require('jsonwebtoken');
const UserModel = require('./model');
const Mailer = require('./mailer');
const UUID = require('uniq-id');
require('dotenv').config()

//Post route for User Model...
const creatUser =   async (req, res) => {
	const { error } = UserModel.validate(req.body)
	if (error) return status(400).send(error.details[0].message)

	let user = await UserModel.User.findOne({ email: req.body.email });
	if (user) return res.status(400).send('User Already Registered..');

	let userResponses = new UserModel.User({
		name: req.body.name,
		email: req.body.email,
		password: req.body.password,
		unique_id: UUID()
	});

	userResponses = await userResponses.save();

	Mailer.sendVerificationMail(userResponses);
	res.send(userResponses);
} 

// function for email comfirmation ...
const confirmEmail = async (req, res) => {
	let verify = await UserModel.User.findOne({ uniqId: req.params.uniqId });
	
	//Difference Counting...
	const today = new Date();																	
	const enteredDate = new Date(verify.created_at);
	const difference = (today - enteredDate);
	//Get Difference in Minutes
	const diffMins = Math.round(((difference % 86400000) % 3600000) / 60000);												

		if (verify.status == 'Unverified') {
			if (diffMins > 3) {
				res.send("<h1>Your Link Was Expired....</h1>")
			} else {
				UserModel.User.findOneAndUpdate({ status: "Unverified" }, { status: "verified" }, (error, data) => {
					if (error) {
						console.log("Your error is" + error)
					} else {
						res.send(data)
						res.send("<h1 style='color:#09752f'>Your Email Is succesfully Verified....</h1>")
					}
				})
			}
		} else {
			res.send("<h1 style='color:red' >You Are Already verified Your EmailId</h1>");
		}
}

//Get by Id route for User Model...
const getUser = async (req, res) => {
	const user = await UserModel.User.findById(req.params.id);
	if (!user) return res.status(404).send('user not found....');

	res.send(user);
};

//Get ALl  route for User Model...
const getAllUser = async (req, res) => {
	const user = await UserModel.User.find() 
	res.send(user);
};

//Login Verification route for User Model...
const login = async (req, res) => {
	let validEmail = await UserModel.User.findOne({ email: req.body.email });
	if (!validEmail) return res.status(400).send('Invalid EmailId --- Aa Tamaru Email id nathi.');

	let validPassword = await UserModel.User.findOne({ password: req.body.password });
	if (!validPassword) return res.status(400).send('Invalid Password ----PAssword to yad rakho bhai');

	const token = jwt.sign({ _id: UserModel.User._id }, process.env.JWT_SECRET)
	res.send(`User login Suceesfully & Your Web Token Is ${token}`);
}

//validToken routes...
const validToken = (req, res, next) => {
	try {

		let token = req.headers.authorization;
		token = token.split(' ')[1]
		if (token) {
			jwt.verify(token, process.env.JWT_SECRET, function (error, decode) {
				if (error) {
					return res.status(401).send('Verification error' + error)
				} else {
					console.log(decode)
					req.decode = decode
					next()
				}
			})
		}
	} catch (error) {
		res.send('invalid Token')
	}
}

//Forget PAssword Routes..
const forgetPassword = async (req ,res ) => {

	const user = await UserModel.User.findOne({email : req.body.email})
	if (!user) return res.status(404).send('You are not Register user Please login first.')
	
	if (user.status == 'Unverified'){
		res.send('Please verify Your mail First..')
	}else{
		res.send('Your password Reset Link is send Suceesdfully on your Register-mail ID')
		Mailer.resetPasswordMail(user) 
	}
}

//Reset PAssword Routes...
const resetPassword = async(req , res) => {
	try{
		const reset = await UserModel.User.findByIdAndUpdate(req.params.id ,{
			password 	: req.body.password 
		} , {new : true} )
		res.send(reset)
	}catch(error){
		res.send('The given id is not true...')
	}	
}


//exporting All modules to 
module.exports = {
	creatUser,
	getUser,
	getAllUser,
	confirmEmail,
	login,
	validToken,
	forgetPassword,
	resetPassword
}