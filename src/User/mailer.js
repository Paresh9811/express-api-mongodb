const nodemailer = require('nodemailer');
require('dotenv').config()


//Creat Transporter..
const transporter = nodemailer.createTransport({
	service : 'gmail',
		auth : {
			user : process.env.EMAILID,																			
			pass :  process.env.EMAIL_PASSWORD
		}
});

//creating function for send mail
const sendVerificationMail = (userResponses) =>{
	//link1 send this link for verification...
	var link1 = `http://localhost:2000/api/users/verifyuser/${userResponses.unique_id}` 									
	
	const mailOption = {
		from        : process.env.EMAILID,
		to          :  userResponses.email ,
		subject     : 'Varification link ',
		html        : `<h1> Hello good after noon MR. ${userResponses.name} </h1> 
					<a href=${link1 }>Hi Please click here to verify... </a>`
	};

	transporter.sendMail(mailOption , function (error , info){
		if(error) {
			console.log(error);
		}
		else{
			console.log('email send successfully :' + info.response);
		}
	})
}

const resetPasswordMail = (user) => {

	//link1 send this link for Reset Password...
	var link2 = 'http://localhost:2000/api/reset/'

	const mailOptions = {
		from		: process.env.EMAILID,
		to			: user.email,
		subject		: 'Passsword reset Link',
		html		: ` <a href = ${link2}${user._id}> Click on meReset Your password</a> <br>
						<a> Kindly copy this link and put int Post man</a> `
	};
	
	transporter.sendMail(mailOptions, function(error, info){
		if (error) {
		console.log(error);
		} else {
		console.log('Email sent' + info.response);
		}
	});	
}


module.exports = { 
	sendVerificationMail , 
	resetPasswordMail
}

