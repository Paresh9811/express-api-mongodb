const TodoModel = require('./model');							
const UserModel = require('../User/model');									

//Post route function for ToDo Model...
const createToDo = async (req, res) => {
	try {
		const { error } = TodoModel.validate(req.body)
		if (error) return res.status(400).send(error.details[0].message)

		let todo = await  UserModel.User.findOne({ _id: req.body.userId });
		if (todo === null) return res.status(400).send('User Id is not registered....');

		let todoResponse = new TodoModel.Todo({
			title       : req.body.title,
			description : req.body.description,
			userId      : req.body.userId
		});

		todoResponse = await todoResponse.save();
		res.send(todoResponse);

	} catch (err) {
		res.send('User Id is not registered....')
	}
}

//GET route function for ToDo Model...
const getAllToDo = async (req ,res) => {
	const todo = await TodoModel.Todo.find()
	res.send(todo);
}

//GET All route function for ToDo Model...
const getToDo = async (req ,res) => {
	try{
		const todo = await TodoModel.Todo.findById(req.params.id);
		res.send(todo);
	}catch(error){
		res.send('The Requested Id is not Found in data base....')
	}
}

//Delete route Function for ToDo Model...
const deleteToDo = async (req,res) =>{
	try{
		const todo = await TodoModel.Todo.findByIdAndRemove(req.params.id);
		res.send(todo)
	}
	catch(error){
		res.send('Sorry given id is not delete because it is not found in data base..')
	}
}

//PUT route Function for ToDo Model...
const updateToDo = async(req,res) => {
	try{

		const todo = await TodoModel.Todo.findByIdAndUpdate(req.params.id , {
			title       : req.body.title,
			description : req.body.description,
			userId      : req.body.userId
		}, { new:true } );
		res.send(todo);
	}
	catch(error){
		res.send('Sorry given Id is not in databas..')
	}
	
}

// Exporting all modules
module.exports = { 
	createToDo ,
	getAllToDo , 
	getToDo , 
	deleteToDo , 
	updateToDo 
}