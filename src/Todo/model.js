const mongoose = require('mongoose');									
const Joi = require('joi');												

//Creating  Todo model
const todoschema    = new mongoose.Schema({
	title : {
		type        : String,
		minlength   : 5,
		maxlength   : 50,
		require     : true
	},

	description: {
		type        : String,
		minlength   : 10,
		maxlength   : 100,
		require     : true
	},

	createdAt:{
		type        : String,
		default     : Date
	},

	updatedAt:{
		type        : String,
		default     : Date
	},
	
	userId :{
		type    : mongoose.Schema.Types.ObjectId,
		ref     : "User"
	}
});

const Todo = mongoose.model('todos' ,todoschema);

//Validating function or validating fields...
const validate = function validateTodo(todo) {
	const schema = Joi.object ({
		title       : Joi.string().min(5).max(50).required(),
		description : Joi.string().min(10).max(100).required(),
		userId      : Joi.string().min(10).max(100).required()
	});
	return schema.validate(todo)
}

module.exports= {
	Todo ,
	validate
}